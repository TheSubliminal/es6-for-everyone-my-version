import View from './view';

class FighterView extends View {
  hoveredElement = null;

  constructor(fighter, handleClick, handleSelect) {
    super();

    this.createFighter(fighter, handleClick, handleSelect);
  }

  createFighter(fighter, handleClick, handleSelect) {
    const { name, source } = fighter;
    const nameElement = this.createName(name, fighter._id, handleSelect);
    const imageElement = this.createImage(source);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter, this.element), false);
    this.element.addEventListener('mouseenter', event => this.handleHover(event));
    this.element.addEventListener('mouseout', event => this.handleMouseOut(event));
  }

  createName(name, fighterId, handleSelect) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.addEventListener('click', event => handleSelect(event, fighterId));
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  handleHover(event) {
    let root = document.getElementById('root');
    let tipElement = this.createElement({
      tagName: 'div',
      className: 'tip'
    });
    root.appendChild(tipElement);
    tipElement.innerHTML = '⬆⬆⬆<br>CLICK ON NAME<br>TO SELECT FIGHTER';

    let topTipCoord = this.getCoords(event.target).top + event.target.offsetHeight;
    if (topTipCoord >= document.body.offsetHeight) {
      topTipCoord = this.getCoords(event.target).top - tipElement.offsetHeight;
      tipElement.innerHTML = 'CLICK ON NAME<br>TO SELECT FIGHTER<br>⬇⬇⬇';
    }
    tipElement.style.top = `${topTipCoord}px`;
    tipElement.style.left = `${(this.getCoords(event.target).left + event.target.offsetWidth/2) - tipElement.offsetWidth / 2}px`;

    this.hoveredElement = event.target;
  }

  getCoords(elem) {
    let box = elem.getBoundingClientRect();

    let docEl = document.documentElement;

    let scrollTop = window.pageYOffset;
    let scrollLeft = window.pageXOffset;

    let clientTop = docEl.clientTop;
    let clientLeft = docEl.clientLeft;

    let top = box.top + scrollTop - clientTop;
    let left = box.left + scrollLeft - clientLeft;

    return {
      top: top,
      left: left
    };
  }

  handleMouseOut(event) {
    let relatedTarget = event.relatedTarget;
    let root = document.getElementById('root');
    if (relatedTarget) {
      while (relatedTarget) {
        if (relatedTarget === this.hoveredElement) return;
        relatedTarget = relatedTarget.parentNode;
      }
    }
    this.hoveredElement = null;

    let tipElement = root.getElementsByClassName('tip')[0];
    if (tipElement) {
      root.removeChild(tipElement);
    }
  }
}

export default FighterView;
