class Fighter {
  name;
  image;
  health;
  attack;
  defense;

  constructor(details) {
    this.name = details.name;
    this.image = details.source;
    this.health = details.health;
    this.attack = details.attack;
    this.defense = details.defense;
  }

  getHitPower() {
    let criticalHitChance = Math.random() + 1;
    return this.attack * criticalHitChance;
  }

  getBlockPower() {
    let dodgeChance = Math.random() + 1;
    return this.defense * dodgeChance;
  }
}

export default Fighter;