import View from './view';
import FighterView from './fighterView';
import DetailsView from './detailsView';
import FightView from './fightView';
import { fighterService } from './services/fightersService';
import Fighter from "./fighter";

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.handleSubmission = this.handleDetailsSubmission.bind(this);
    this.handleSelection = this.handleSelectFighter.bind(this);
    this.handleDeletion = this.handleDeleteFighter.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  selectedFighters = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.handleSelection);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async obtainDetails(fighterId) {
    let details;
    if (!this.fightersDetailsMap.has(fighterId)) {
    details = await fighterService.getFighterDetails(fighterId);
    this.fightersDetailsMap.set(fighterId, details);
    } else {
      details = this.fightersDetailsMap.get(fighterId);
    }
    return details;
  }

  async handleFighterClick(event, fighter, fighterElement) {
    let details = await this.obtainDetails(fighter._id);

    const detailsView = new DetailsView(details, fighterElement, this.handleSubmission, this.handleDeletion);
    detailsView.createDetailsWindow(details);
  }

  handleDetailsSubmission(event, fighterId) {
    event.preventDefault();
    let fighterDetails = this.fightersDetailsMap.get(fighterId);

    [...event.target.elements]
      .filter(elem => elem.name && elem.value >= 0)
      .forEach(elem => {
        fighterDetails[elem.name] = elem.value;
      });

    fighterService.modifyFighter(fighterId, fighterDetails);
    this.fightersDetailsMap.set(fighterId, fighterDetails);
  }

  handleSelectFighter(event, fighterId) {
    event.stopPropagation();

    if (this.selectedFighters.has(event.target)) {
      event.target.style.backgroundColor = '';
      this.selectedFighters.delete(event.target);
    } else {
      switch (this.selectedFighters.size) {
        case 0:
          event.target.style.backgroundColor = 'red';
          break;
        case 1:
          if ([...this.selectedFighters.keys()][0].style.backgroundColor === 'red'){
            event.target.style.backgroundColor = 'blue';
          } else {
            event.target.style.backgroundColor = 'red';
          }
          break;
        case 2:
          let previousFighterElement = [...this.selectedFighters.keys()][this.selectedFighters.size - 1];
          previousFighterElement.style.backgroundColor = '';

          this.selectedFighters.delete(previousFighterElement);
          if ([...this.selectedFighters.keys()][0].style.backgroundColor === 'red'){
            event.target.style.backgroundColor = 'blue';
          } else {
            event.target.style.backgroundColor = 'red';
          }
          break;
      }
      this.selectedFighters.set(event.target, fighterId);
    }
    if (this.selectedFighters.size === 2) {
      this.showFightButton();
    } else {
      this.removeFightButton();
    }
  }

  handleDeleteFighter(fighterId) {
    this.fightersDetailsMap.delete(fighterId);
    fighterService.deleteFighter(fighterId);
  }

  showFightButton() {
    let button = document.getElementById('fight-start-button');
    if (!button) {
      let fightStartButton = this.createElement({
        tagName: 'button',
        className: 'fight-start-button'
      });
      fightStartButton.id = 'fight-start-button';
      fightStartButton.innerText = 'FIGHT!!!';
      fightStartButton.addEventListener('click', () => {
        this.fight(...this.selectedFighters.values());
      });

      this.element.append(fightStartButton);
    }
  }

  removeFightButton() {
    let button = document.getElementById('fight-start-button');
    if (button) {
      button.parentElement.removeChild(button);
    }
  }

  async fight(fighterAId, fighterBId) {
    let fighterADetails = await this.obtainDetails(fighterAId);
    let fighterBDetails = await this.obtainDetails(fighterBId);
    let fighterA = new Fighter(fighterADetails);
    let fighterB = new Fighter(fighterBDetails);
    const fighting = new FightView(fighterA, fighterB);

    fighting.fight();
  }
}

export default FightersView;
