const API_URL = 'https://bsa-nodejs-my-version.herokuapp.com';

function getFromApi(endpoint) {
  const url = API_URL + endpoint;
  const options = {
    method: 'GET'
  };

  return fetch(url, options)
    .then(response => {
      if (response.ok) {
        return response.text();
      } else {
        return Promise.reject(Error('Failed to load'));
      }
    })
    .catch(error => { throw error });
}

function postToApi(endpoint, data) {
  const url = API_URL + endpoint;
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: data
  };

  return fetch(url, options)
    .then(response => {
      if (!response.ok) {
        return Promise.reject(Error('Failed to post'));
      }
    })
    .catch(error => { throw error });
}

function putToApi(endpoint, data) {
  const url = API_URL + endpoint;
  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: data
  };

  return fetch(url, options)
    .then(response => {
      if (!response.ok) {
        return Promise.reject(Error('Failed to modify'));
      }
    })
    .catch(error => { throw error });
}

function deleteFromApi(endpoint) {
  const url = API_URL + endpoint;
  const options = {
    method: 'DELETE'
  };

  return fetch(url, options)
    .then(response => {
      if (!response.ok) {
        return Promise.reject(Error('Failed to delete'));
      }
    })
    .catch(error => { throw error });
}

export { getFromApi, postToApi, putToApi, deleteFromApi }