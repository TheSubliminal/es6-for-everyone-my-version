import View from './view';
import { fightHistoryService } from './services/fightHistoryService';

class FightView extends View {
  fighterA;
  fighterB;

  constructor(fighterA, fighterB) {
    super();

    this.fighterA = fighterA;
    this.fighterB = fighterB;
  }

  createBattleField() {
    this.element = this.createElement({
      tagName: 'div',
      className: 'battle-container'
    });
    const fighterAContainer = this.createElement({
      tagName: 'div',
      className: 'fighter-a-container'
    });

    const fighterAImage = this.createElement({
      tagName: 'img',
      className: 'fighter-a-image',
      attributes: { src: this.fighterA.image }
    });
    fighterAContainer.append(fighterAImage);

    const fighterALabel = this.createElement({
      tagName: 'div',
      className: 'fighter-a-label'
    });

    const fighterAHpIndicator = this.createElement({
      tagName: 'span',
      className: 'hp-indicator',
      attributes: {
        id: 'hp-a'
      }
    });
    fighterAHpIndicator.innerText = '100%HP';

    fighterALabel.innerText = this.fighterA.name;
    fighterALabel.append(fighterAHpIndicator);
    fighterAContainer.append(fighterAImage, fighterALabel);

    const fighterBContainer = this.createElement({
      tagName: 'div',
      className: 'fighter-b-container'
    });

    const fighterBImage = this.createElement({
      tagName: 'img',
      className: 'fighter-b-image',
      attributes: { src: this.fighterB.image }
    });

    const fighterBLabel = this.createElement({
      tagName: 'div',
      className: 'fighter-b-label'
    });

    const fighterBHpIndicator = this.createElement({
      tagName: 'span',
      className: 'hp-indicator',
      attributes: {
        id: 'hp-b'
      }
    });
    fighterBHpIndicator.innerText = '100%HP';

    fighterBLabel.innerText = this.fighterB.name;
    fighterBLabel.appendChild(fighterBHpIndicator);
    fighterBContainer.append(fighterBImage, fighterBLabel);

    this.element.append(fighterAContainer, fighterBContainer);

    document.getElementsByClassName('fighters')[0].style.display = 'none';
  }

  createHitLabel(damage, defendingFighter) {
    let hitLabel = this.createElement({
      tagName: 'div',
      className: 'hit-label'
    });
    if (damage > 0) {
      hitLabel.innerText = `-${damage.toFixed(2)}HP`;
      hitLabel.style.backgroundColor = 'red';
    } else {
      hitLabel.innerText = 'Blocked!';
      hitLabel.style.backgroundColor = 'lightblue';
    }

    let container = this.getFighterContainer(defendingFighter);
    container.appendChild(hitLabel);

    let topCoordinate = Math.random() * (container.clientHeight - hitLabel.offsetHeight);
    let leftCoordinate = Math.random() * (container.clientWidth - hitLabel.offsetWidth);
    hitLabel.style.top = `${topCoordinate}px`;
    hitLabel.style.left = `${leftCoordinate}px`;
    setTimeout(() => {
      container.removeChild(hitLabel)
    }, 750);
  }

  fight() {
    const root = document.getElementById('root');
    this.createBattleField();
    root.append(this.element);

    const fighters = [this.fighterA, this.fighterB];
    let fighterSwitch = Math.round(Math.random());

    const startingHealthA = this.fighterA.health;
    const startingHealthB = this.fighterB.health;

    const hpIndicatorA = document.getElementById('hp-a');
    const hpIndicatorB = document.getElementById('hp-b');

    let fightTimer = setInterval(() => {
      let defendingFighter = fighters[+fighterSwitch];
      let attackingFighter = fighters[+!fighterSwitch];

      let hitPower = attackingFighter.getHitPower();
      let blockPower = defendingFighter.getBlockPower();
      if (hitPower < blockPower) {
        this.createHitLabel(0, defendingFighter);
      } else {
        let damage = hitPower - blockPower;
        this.createHitLabel(damage, defendingFighter);
        defendingFighter.health -= damage;
      }

      hpIndicatorA.innerText = `${Math.ceil(this.fighterA.health / startingHealthA * 100)}%HP`;
      hpIndicatorB.innerText = `${Math.ceil(this.fighterB.health / startingHealthB * 100)}%HP`;

      if (this.fighterA.health < 0 || this.fighterB.health < 0) {
        clearInterval(fightTimer);
        this.clearHitLabels();

        let loser = fighters.filter(fighter => fighter.health < 0)[0];
        let loserElem = this.getFighterContainer(loser);
        loserElem.parentElement.removeChild(loserElem);

        const fightData = {
          fighterA: this.fighterA.name,
          fighterB: this.fighterB.name,
          winner: loser.name === this.fighterA.name ? this.fighterB.name : this.fighterA.name,
          date: new Date().toLocaleString()
        };
        fightHistoryService.postFight(fightData);

        let overlayKO = this.createKnockOutOverlay();
        root.appendChild(overlayKO);

        let visible = true;
        let overlayTimer = setInterval(() => {
          if (visible) {
            overlayKO.style.visibility = 'visible';
          } else {
            overlayKO.style.visibility = 'hidden';
          }
          visible = !visible;
        }, 450);

        setTimeout(() => {
          clearInterval(overlayTimer);
          root.removeChild(this.element);
          root.removeChild(overlayKO);
          document.getElementsByClassName('fighters')[0].style.display = 'flex';
        }, 6000);
      }

      fighterSwitch = !fighterSwitch;
    }, 1000);

  }

  getFighterContainer(fighter) {
    let container;
    if (fighter.name === this.fighterA.name) {
      container = document.getElementsByClassName('fighter-a-container')[0];
    } else {
      container = document.getElementsByClassName('fighter-b-container')[0];
    }
    return container;
  }

  createKnockOutOverlay(){
    let overlay = this.createElement({
      tagName: 'div',
      className: 'knockout-overlay'
    });
    overlay.innerText = 'WINNER';

    return overlay;
  }

  clearHitLabels() {
    let hitLabels = document.getElementsByClassName('hit-label');
    [...hitLabels].forEach(label => {
      label.parentElement.removeChild(label);
    });
  }
}

export default FightView;