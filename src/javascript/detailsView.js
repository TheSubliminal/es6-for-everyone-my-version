import View from './view';

class DetailsView extends View {
  details;
  fighterElement;
  onsubmit;
  ondelete;

  constructor(details, fighterElement, submitHandler, deleteHandler) {
    super();

    this.details = details;
    this.fighterElement = fighterElement;
    this.onsubmit = submitHandler;
    this.ondelete = deleteHandler;
  }

  createDetailsWindow() {
    const root = document.getElementById('root');
    const body = document.body;

    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter-details'
    });

    let detailsForm = this.createForm();

    this.element.append(detailsForm);
    this.element.style.top = `${window.scrollY}px`;
    root.append(this.element);

    body.style.overflow = 'hidden';
  }

  createForm() {
    const root = document.getElementById('root');
    const body = document.body;
    let { attack, defense, health, name, _id: id } = this.details;
    let stats = {attack, defense, health};
    let detailsForm = this.createElement({
      tagName: 'form',
      className: 'fighter-details-form'
    });
    detailsForm.addEventListener('submit', event => this.onsubmit(event, id));

    let nameLabel = this.createElement({
      tagName: 'div',
      className: 'fighter-name'
    });
    nameLabel.innerText = `${name}'s stats:`;
    
    detailsForm.append(nameLabel);

    let submitButton = this.createElement({
      tagName: 'input',
      className: 'form-button',
      attributes: {
        type: 'submit',
        value: 'Save changes',
        id: 'submit-btn'
      }
    });

    Object.keys(stats).forEach(attrName => {
      let formRow = this.createFormRow();

      let attrLabel = this.createAttributeLabel(attrName);
      let attrInput = this.createAttributeInput(attrName, stats[attrName], submitButton);

      formRow.append(attrLabel, attrInput);
      detailsForm.append(formRow);
    });

    let buttonRow = this.createFormRow();

    let closeButton = this.createElement({
      tagName: 'button',
      className: 'form-button'
    });
    closeButton.innerText = 'Close';
    closeButton.addEventListener('click', () => {
      root.removeChild(this.element);
      body.style.overflow = '';
    });

    let deleteButtonRow = this.createFormRow();
    deleteButtonRow.classList.add('delete-button-row');

    let deleteFighterButton = this.createElement({
      tagName: 'button',
      className: 'delete-button'
    });
    deleteFighterButton.innerText = 'Delete fighter';
    deleteFighterButton.addEventListener('click', () => {
      this.ondelete(id);
      this.fighterElement.parentElement.removeChild(this.fighterElement);
      root.removeChild(this.element);
    });

    buttonRow.append(closeButton, submitButton);
    deleteButtonRow.append(deleteFighterButton);
    detailsForm.append(buttonRow, deleteButtonRow);
    return detailsForm;
  }

  createFormRow() {
    return this.createElement({
      tagName: 'div',
      className: 'form-row'
    });
  }

  createAttributeLabel(attributeName) {
    let label = this.createElement({
      tagName: 'label',
      className: 'attr-label',
      attributes: {
        ['for']: `${attributeName}-data`
      }
    });
    label.innerText = `${attributeName[0].toUpperCase() + attributeName.slice(1)}:`;
    return label;
  }

  createAttributeInput(attributeName, attributeValue, buttonElement) {
    let input = this.createElement({
      tagName: 'input',
      className: 'attr-input',
      attributes: {
        type: 'number',
        name: attributeName,
        value: attributeValue,
        id: `${attributeName}-data`
      }
    });
    input.addEventListener('input', () => {
      buttonElement.style.visibility = 'visible';
    });
    return input;
  }
}

export default DetailsView;