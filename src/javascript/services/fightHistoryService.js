import { postToApi } from '../helpers/apiHelper';

class FightHistoryService {
  async postFight(data) {
    try {
      const endpoint = `/fights`;
      await postToApi(endpoint, JSON.stringify(data));
    } catch (error) {
      throw error;
    }
  }
}

export const fightHistoryService = new FightHistoryService();