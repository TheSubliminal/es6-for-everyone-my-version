import { getFromApi, putToApi, deleteFromApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = '/user';
      const apiResult = await getFromApi(endpoint);

      return JSON.parse(apiResult);
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `/user/${_id}`;
      const apiResult = await getFromApi(endpoint);

      return JSON.parse(apiResult);
    } catch (error) {
      throw error;
    }
  }

  async modifyFighter(_id, data) {
    try {
      const endpoint = `/user/${_id}`;
      await putToApi(endpoint, JSON.stringify(data));
    } catch (error) {
      throw error;
    }
  }

  async deleteFighter(_id) {
    try {
      const endpoint = `/user/${_id}`;
      await deleteFromApi(endpoint);
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();